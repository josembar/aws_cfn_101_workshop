# README #

### What is this repository for? ###

* This repository containts the exercises proposed on https://cfn101.workshop.aws/

### How do I get set up? ###

* The CFN templates can be deployed via AWS console or AWS CLI.
* For the nested stacks exercise you need to upload the templates in part_02/nested_stacks to a s3 bucket. All except main.yml. The CFN template that will reference the other ones is main.yml. 

### Who do I talk to? ###

* jose.barrantes@hotmail.es